# luxor-interview
This is a pycharm project.

Two python files.
1. HistoricHash.py
2. TestHistoricHash.py

## HistoricHash.py

This contains the Python Flask code necessary to handle URLs.
The two URLs we handle here are:
* /hashes
* /btcusd

When you run HistoricHash.py, it will spin up a web server at 127.0.0.1 port 5000.
You then load a URL like http://127.0.0.1:5000/hashes in your web browser

### /hashes

This API only supports GET & POST.

GET requires 2 input arguments and allows an optional 3rd.

Input:
* from - This is a unix timestamp (in UTC) for the range of data you'd like to query
* to - This is a unix timestamp (in UTC) for the range of data you'd like to query
* (Optional) account - This is the account whose hash rate you'd like to query.

GET Output:

This method returns JSON
* success: true | false
* (Optional) duration: Number of seconds between to & from (inclusive)
* (Optional) ghs: Average hashrate reported over this time interval
* (Optional) total_gh: Total hashrate reported over this time interval
* (Optional) error: String indicating what went wrong

POST requires two input arguments.

Input:
* gh - This is number of GH being reported.
* account - A string indicating which account/subaccount is reporting this hashrate

POST Output:

This method returns JSON
* success: true | false
* (Optional) error: String indicating what went wrong

### /btcusd
This API only supports GET & POST & PUT.
The design here is we could also create "/btceur" or "/btcgbp" and there would be very few differences required in the code.

GET requires 2 input arguments
* from - This is a unix timestamp (in UTC) for the range of data you'd like to query
* to - This is a unix timestamp (in UTC) for the range of data you'd like to query

GET Output:

This method returns JSON
* success: true | false
* (Optional) currency: The currency whose values have been queried (theoretically could support arbitrary fiat currencies)
* (Optional) exchange_rate: json array of:
  * timestamp: the timestamp of the value reading
  * value: the fiat value (in smallest units possible, such as pennies)
* (Optional) error: String indicating what went wrong

POST requires 1 input arguments
* exchange_rate: Fiat value of BTC (in smallest units possible, such as pennies)

POST Output:
This method returns JSON
* success: true | false
* (Optional) error: String indicationg what went wrong

PUT works the same as POST, but it will overwrite existing rows.
At first I was going to have the UTC timestamps be supplied as an input argument, so a re-write API made sense, but as I started documenting the code I decided to take the timestamnps from the clock at the time the API call was made. There are tradeoffs here. We can talk about them.

## TestHistoricHash.py

The is a small collection of Unit Tests for HistoricHash. I did not get too involved here, but a variety of scenarios could be modeled out if this were going to become production software.


import pytest
import HistoricHash
import time
from datetime import datetime, timezone


@pytest.fixture()
def app():
    HistoricHash.app.config.update({
        "TESTING": True,
    })
    app = HistoricHash.create_app()

    yield app

    # clean up / reset resources here
    HistoricHash.cleanup_app()


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def runner(app):
    return app.test_cli_runner()


def test_post_hashes(client):
    response = client.post("/hashes", data={"gh": "100999", "account": "satoshi"})
    assert b'"success": true' in response.data


def test_get_hashes(client):
    start_ts = datetime.now(timezone.utc)
    response = client.post("/hashes", data={"gh": "100", "account": "satoshi"})
    assert b'"success": true' in response.data
    time.sleep(1)  # we want a second timestamp for this next sample
    response = client.post("/hashes", data={"gh": "100", "account": "satoshi"})
    assert b'"success": true' in response.data
    end_ts = datetime.now(timezone.utc)

    response = client.get("/hashes", query_string={
        "from": int(start_ts.timestamp()),
        "to": int(end_ts.timestamp()),
        "account": "satoshi"})
    assert b'"success": true' in response.data


def test_post_btcusd(client):
    start_ts = datetime.now(timezone.utc)
    response = client.post("/btcusd", data={"exchange_rate": "1700001"})
    assert b'"success": true' in response.data
    time.sleep(1)  # we want a second timestamp for this next sample
    end_ts = datetime.now(timezone.utc)

    # double check that we were able to update the value
    response = client.get("/btcusd", query_string={
        "from": int(start_ts.timestamp()),
        "to": int(end_ts.timestamp()),
    })
    assert b'"success": true' in response.data
    assert b'1700001' in response.data


def test_get_btcusd(client):
    response = client.get("/btcusd", query_string={"from": "1", "to": "2"})
    assert b'"success": true' in response.data

from flask import Flask, request, json
from flask_restful import Resource, Api
import psycopg
from datetime import datetime, timezone

app = Flask(__name__)


# A helper function for being Post/Get agnostic
# Just pulls the input-dictionary (query or post body) from the user
def request_input():
    if request.method == "POST":
        return request.form
    elif request.method == "PUT":
        return request.form
    elif request.method == "GET":
        return request.args
    elif request.method == "DELETE":
        return request.args
    else:
        return request.args


# A helper function to pull a string out of the request
# If it is not present it gives us an error response for sending to the client
def string_argument(name):
    value = request_input().get(name)
    if value is None or len(value) == 0:
        print('Missing ' + name + ' argument.', request_input().to_dict())
        response = {'success': False, 'error': "Supply " + name + " as a non-empty string"}
        response = app.response_class(
            response=json.dumps(response),
            status=400,
            mimetype='application/json'
        )
        return None, response
    else:
        return value, None


# A helper function to pull a string out of the request
# If it is not present it returns the supplied default value
def string_argument_with_default(name, default):
    value = request_input().get(name)
    if value is None or len(value) == 0:
        return default, None
    else:
        return value, None


# A helper function to pull an integer out of the request
# If it is not present it gives us an error response for sending to the client
def int_argument(name):
    value = request_input().get(name)
    if value is None or int(value) <= 0:
        print('Missing ' + name + ' argument.', request_input().to_dict())
        response = {'success': False, 'error': "Supply " + name + " as a positive integer"}
        response = app.response_class(
            response=json.dumps(response),
            status=400,
            mimetype='application/json'
        )
        return None, response
    else:
        return int(value), None


# A wrapper around the row we want to insert
# By creating a dedicated class out of the row
# we can extend the database row without changing the API
# we can also more easily replace the datastore with a mock object
class HashRow:
    def __init__(self, account, gh):
        self.account = account
        self.gh = gh
        self.timestamp = datetime.now(timezone.utc)


# A wrapper around the query we want run
# By creating a dedicated class out of the query
# we can extend the database row without changing the API
# we can also more easily replace the datastore with a mock object
class HashQuery:
    def __init__(self, from_ts, to_ts, account):
        self.from_ts = datetime.utcfromtimestamp(from_ts)
        self.to_ts = datetime.utcfromtimestamp(to_ts)
        self.account = account


# The Hashes request object
# or fetching or setting "who" hashed "how much" and "when"
# Only GET & POST are supported
class Hashes(Resource):
    def __init__(self, datasource, datastore):
        self.datasource = datasource
        self.datastore = datastore

    def get(self):
        from_ts, error_response = int_argument("from")
        if error_response is not None:
            return error_response
        to_ts, error_response = int_argument("to")
        if error_response is not None:
            return error_response
        account, error_response = string_argument_with_default("account", None)
        if error_response is not None:
            return error_response

        # flip to/from
        if from_ts > to_ts:
            from_ts, to_ts = to_ts, from_ts

        print("Querying", from_ts, "to", to_ts, "account", account)
        query = HashQuery(from_ts, to_ts, account)
        total_hash, error_value = self.datasource.run_hash_query(query)

        response_code = 200
        if error_value is not None:
            response = {'success': False, 'error': error_value}
            response = app.response_class(
                response=json.dumps(response),
                status=response_code,
                mimetype='application/json'
            )
            return response
        else:
            # +1 to account for these timestamps being inclusive
            total_seconds = ((to_ts - from_ts) + 1)
            ghs = total_hash / total_seconds

            response = {'success': True, "ghs": ghs, "total_gh": total_hash, "duration": total_seconds}
            response = app.response_class(
                response=json.dumps(response),
                status=response_code,
                mimetype='application/json'
            )
            return response

    def post(self):
        gh, error_response = int_argument("gh")
        if error_response is not None:
            return error_response
        account, error_response = string_argument("account")
        if error_response is not None:
            return error_response

        row = HashRow(account, gh)
        error_value = self.datastore.add_hash_row(row)

        response_code = 200
        if error_value is not None:
            response = {'success': False, 'error': error_value}
            response = app.response_class(
                response=json.dumps(response),
                status=response_code,
                mimetype='application/json'
            )
            return response
        else:
            response = {'success': True}
            response = app.response_class(
                response=json.dumps(response),
                status=response_code,
                mimetype='application/json'
            )
            return response


# A wrapper around the row we want to insert
# By creating a dedicated class out of the row
# we can extend the database row without changing the API
# we can also more easily replace the datastore with a mock object
class ExchangeQuery:
    def __init__(self, from_ts, to_ts, currency):
        self.from_ts = datetime.utcfromtimestamp(from_ts)
        self.to_ts = datetime.utcfromtimestamp(to_ts)
        self.currency = currency


# A wrapper around the query we want run
# By creating a dedicated class out of the query
# we can extend the database row without changing the API
# we can also more easily replace the datastore with a mock object
class ExchangeRateRow:
    def __init__(self, exchange_rate, currency):
        self.currency = currency
        self.exchange_rate = exchange_rate
        self.timestamp = datetime.now(timezone.utc)
        self.allow_upsert = False


# The Exchange Rate request object
# or fetching or setting "how much" is "what" worth and "when"
# Only GET & POST & PUT are supported
class ExchangeRate(Resource):
    def __init__(self, datasource, datastore, currency):
        self.datasource = datasource
        self.datastore = datastore
        self.currency = currency

    # def get(self):
    # data = pd.read_csv('users.csv')  # read CSV
    # data = data.to_dict()  # convert dataframe to dictionary
    # return {'data': "abc"}, 200  # return data and 200 OK code
    def get(self):
        from_ts, error_response = int_argument("from")
        if error_response is not None:
            return error_response
        to_ts, error_response = int_argument("to")
        if error_response is not None:
            return error_response

        print("Querying", from_ts, to_ts)
        query = ExchangeQuery(from_ts, to_ts, self.currency)
        value, error_value = self.datastore.run_exchange_query(query)

        response_code = 200
        if error_value is not None:
            response = {'success': False, 'error': error_value}
            response = app.response_class(
                response=json.dumps(response),
                status=response_code,
                mimetype='application/json'
            )
            return response
        else:
            response = {'success': True, "currency": self.currency, "exchange_rate": value}
            response = app.response_class(
                response=json.dumps(response),
                status=response_code,
                mimetype='application/json'
            )
            return response

    def post(self):
        return self.insert(False)

    def put(self):
        return self.insert(True)

    def insert(self, allow_upsert):
        exchange_rate, error_response = int_argument("exchange_rate")
        if error_response is not None:
            return error_response

        row = ExchangeRateRow(exchange_rate, self.currency)
        row.allow_upsert = allow_upsert
        error_value = self.datastore.add_exchange_rate_row(row)

        response_code = 200
        if error_value is not None:
            response = {'success': False, 'error': error_value}
            response = app.response_class(
                response=json.dumps(response),
                status=response_code,
                mimetype='application/json'
            )
            return response
        else:
            response = {'success': True}
            response = app.response_class(
                response=json.dumps(response),
                status=response_code,
                mimetype='application/json'
            )
            return response


# Class for wrapping the postgres database
# The idea here is that we could subclass or mock this object
# or implement it as another database type
class PostgreDatabase:
    def __init__(self):
        self.db_name = "luxor"
        self.db_username = "luxor"
        self.db_password = "luxor"
        self.db_host = "localhost"
        self.db_port = 5432

    def connection(self):
        return psycopg.connect(
            host=self.db_host,
            dbname=self.db_name,
            user=self.db_username,
            password=self.db_password
        )

    def run_exchange_query(self, exchange_query):
        error_value = None
        db_connection = None
        cur = None
        response_array = []
        try:
            db_connection = self.connection()
            cur = db_connection.cursor()
            cur.execute(
                "SELECT timestamp, units_per_btc FROM exchangerate WHERE (timestamp >= %s AND timestamp <= %s) AND currency = %s ORDER BY timestamp ASC;",
                (exchange_query.from_ts, exchange_query.to_ts, exchange_query.currency))
            print("Fetching", exchange_query.from_ts, "to", exchange_query.to_ts, exchange_query.currency)

            rows = cur.fetchall()
            for row in rows:
                if row[0] is not None:
                    response_array.append({"timestamp": row[0], "value": row[1]})
                    # print("Fetched", row)
            cur.close()
            cur = None

        except (Exception, psycopg.DatabaseError) as error:
            print(error)
            error_value = str(error)
        finally:
            if cur is not None:
                cur.close()
            if db_connection is not None:
                db_connection.close()

        return response_array, error_value

    def add_exchange_rate_row(self, exchange_rate_row):
        error_value = None
        db_connection = None
        cur = None
        try:
            db_connection = self.connection()
            cur = db_connection.cursor()
            if exchange_rate_row.allow_upsert:
                cur.execute("INSERT INTO exchangerate(timestamp, units_per_btc, currency) VALUES(%s, %s, %s) " +
                            "ON CONFLICT (currency, timestamp)" +
                            "DO UPDATE SET units_per_btc = EXCLUDED.units_per_btc;",
                            (exchange_rate_row.timestamp, exchange_rate_row.exchange_rate, exchange_rate_row.currency))
            else:
                cur.execute("INSERT INTO exchangerate(timestamp, units_per_btc, currency) VALUES(%s, %s, %s); ",
                            (exchange_rate_row.timestamp, exchange_rate_row.exchange_rate, exchange_rate_row.currency))

            print("Inserting", exchange_rate_row.exchange_rate,
                  "at", exchange_rate_row.timestamp,
                  "--", int(round(exchange_rate_row.timestamp.timestamp())))
            cur.close()
            cur = None
            db_connection.commit()

        except (Exception, psycopg.errors.UniqueViolation) as error:
            print(error)
            error_value = "Exchange rate at timestamp already recorded."
        except (Exception, psycopg.DatabaseError) as error:
            print(error)
            error_value = str(error)
        finally:
            if cur is not None:
                cur.close()
            if db_connection is not None:
                db_connection.close()

        return error_value

    def run_hash_query(self, hash_query):
        error_value = None
        db_connection = None
        cur = None
        hash_count = 0
        try:
            db_connection = self.connection()
            cur = db_connection.cursor()
            if hash_query.account is not None:
                cur.execute(
                    "SELECT sum(gh) FROM hashrate WHERE (timestamp >= %s AND timestamp <= %s) AND account = %s;",
                    (hash_query.from_ts, hash_query.to_ts, hash_query.account))
                print("Fetching", hash_query.from_ts, "to", hash_query.to_ts, hash_query.account)

            else:
                cur.execute("SELECT sum(gh) FROM hashrate WHERE (timestamp >= %s AND timestamp <= %s);",
                            (hash_query.from_ts, hash_query.to_ts))
                print("Fetching", hash_query.from_ts, "to", hash_query.to_ts)

            rows = cur.fetchall()
            for row in rows:
                if row[0] is not None:
                    hash_count = row[0]
                    # print("Fetched", row)
                break
            cur.close()
            cur = None

        except (Exception, psycopg.DatabaseError) as error:
            print(error)
            error_value = str(error)
        finally:
            if cur is not None:
                cur.close()
            if db_connection is not None:
                db_connection.close()

        return hash_count, error_value

    def add_hash_row(self, hash_row):
        error_value = None
        db_connection = None
        cur = None
        try:
            db_connection = self.connection()
            cur = db_connection.cursor()
            cur.execute("INSERT INTO hashrate(timestamp, gh, account) VALUES(%s, %s, %s);",
                        (hash_row.timestamp, hash_row.gh, hash_row.account))

            print("Inserting", hash_row.gh,
                  "at", hash_row.timestamp,
                  "--", int(round(hash_row.timestamp.timestamp())))
            cur.close()
            cur = None
            db_connection.commit()

        except (Exception, psycopg.DatabaseError) as error:
            print(error)
            error_value = str(error)
        finally:
            if cur is not None:
                cur.close()
            if db_connection is not None:
                db_connection.close()

        return error_value

    def create_tables(self):
        db_connection = None
        try:
            db_connection = self.connection()
            # create a cursor
            cur = db_connection.cursor()

            # execute a statement
            create_statements = [
                "CREATE TABLE IF NOT EXISTS hashrate (id BIGSERIAL, timestamp TIMESTAMP NOT NULL, account VARCHAR(255) NOT NULL, gh BIGINT NOT NULL);",
                "CREATE INDEX IF NOT EXISTS hashrate_timestamp_idx ON hashrate(timestamp ASC);",
                "CREATE UNIQUE INDEX IF NOT EXISTS hashrate_account_timestamp_idx ON hashrate(account, timestamp);",
                # More indices if we want to support searching by hashrate max/min or exchange rate above/below
                # "CREATE INDEX IF NOT EXISTS gh ON hashrate(gh ASC);",
                # "CREATE INDEX IF NOT EXISTS hashrate_account ON hashrate(account ASC);",
                "CREATE TABLE IF NOT EXISTS exchangerate (id BIGSERIAL, timestamp TIMESTAMP NOT NULL, units_per_btc INT NOT NULL, currency VARCHAR(16) NOT NULL );",
                "CREATE UNIQUE INDEX IF NOT EXISTS exchangerate_currency_timestamp_idx ON exchangerate(currency, timestamp ASC);"
            ]
            if app.config["TESTING"]:
                create_statements.insert(0, "DROP TABLE IF EXISTS hashrate;")
                create_statements.insert(0, "DROP TABLE IF EXISTS exchangerate;")

            for statement in create_statements:
                cur.execute(statement)
                print("Executed: ", statement)

            # close the communication with the PostgreSQL
            cur.close()
            db_connection.commit()
        except (Exception, psycopg.DatabaseError) as error:
            print(error)
        finally:
            if db_connection is not None:
                db_connection.close()
                print('Database connection closed.')

    def reset_database(self):
        db_connection = None
        try:
            db_connection = self.connection()
            # create a cursor
            cur = db_connection.cursor()

            # execute a statement
            create_statements = [
                "DROP TABLE IF EXISTS hashrate;",
                "DROP TABLE IF EXISTS exchangerate;"
            ]

            for statement in create_statements:
                cur.execute(statement)
                print("Executed: ", statement)

            # close the communication with the PostgreSQL
            cur.close()
            db_connection.commit()
        except (Exception, psycopg.DatabaseError) as error:
            print(error)
        finally:
            if db_connection is not None:
                db_connection.close()
                print('Database connection closed.')


database = PostgreDatabase()


def cleanup_app():
    database.reset_database()


# wrapper function to make unit testing easier/possible
def create_app():
    api = Api(app)

    database.create_tables()

    api.add_resource(Hashes, '/hashes',
                     resource_class_kwargs={'datasource': database, 'datastore': database})

    api.add_resource(ExchangeRate, '/btcusd',
                     resource_class_kwargs={'datasource': database, 'datastore': database, 'currency': 'USD'})
    return app


if __name__ == '__main__':
    app = create_app()
    app.run(debug=True)
